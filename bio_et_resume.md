Résumé
------

Le Metalab de la Société des arts technologiques et l’Orchestre Symphonique de Montréal conduisent présentement des travaux de recherche visant à explorer la création d'espaces immersifs permettant la navigation en direct de participant.e.s dans le paysage sonore produit par l'orchestre. La présentation sera composée de démonstrations illustrant les travaux de captation sonore, de rendu de navigation virtuelle en 6 degrés de liberté, de captation/synthèse de l'acoustique de la Maison Symphonique de Montréal et de prototypage d'expériences immersives.   

Bio
---

Nicolas Bouillot est codirecteur de recherche à la Société des arts technologiques (SAT). Après avoir obtenu un doctorat du Conservatoire National des Arts et Métiers de Paris (CNAM-CEDRIC) en 2006, il est devenu chercheur postdoctoral à l'Université McGill. En support à ses recherches, il a initié SATIE (un moteur de rendu audio 3D pour des configurations de haut-parleurs hétérogènes) et Audiodice (groupe de haut-parleurs immersifs).