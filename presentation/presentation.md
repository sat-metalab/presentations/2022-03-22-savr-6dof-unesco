---
title:
- Navigation immersive sonore et collective
author:
- Nicolas Bouillot <br>incluant les travaux de <br>Zack Settel, Jean-Yves Münch, Carl Talbot
institute:
- Société des arts technologiques
theme:
- moon
date:
- 22 mars 2022
aspectratio:
- 169
navigation:
- empty
section-titles:
- false
output:
  revealjs::revealjs_presentation:
    css: styles.css
---

## La [SAT]

* Organisation à But Non Lucratif depuis 1996
* Expertise : Art \& Immersion \& Téléprésence
* Montréal, Quartier des Spectacles

![sat -image-no-border](img/SAT-batiment.png){width=60%}

# Le programme SAV+R (2019-2022)

## SAV+R: Simulation Acoustique Volumétrique en AR/MR/XR 

![BretezDavid -image-no-border](./img/Bretez-David.png){width=35%}
![BretezMichal -image-no-border](./img/Bretez-Michal.png){width=35%}

## 6 degrées de liberté (6-ddl) ?

* l'audition de musique préenregistrée se fait habituellement depuis un point fixe
    * pas de mouvement = 0-ddl
* 6-ddl : l'auditeur peut se déplacer
    * 3 axes de rotations + 3 axes de translations = 6-ddl

## Motivations

* Pédagogie
* Analyse musicale
* Curiosité
* Expérience immersive

## Capture audio pour navigation à 6-ddl

![](img/savr_micro.jpg){width=40%}
![](./img/icsa_centech.jpg){width=40%}

Comment utiliser les microphones à 0-ddl et à 3-ddl afin d'optimiser la simulation acoustique à 6-ddl ?

## Microphones mono

<iframe src="https://player.vimeo.com/video/497051293?color=ffffff&title=0&byline=0&portrait=0" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/497051293">Naviguer dans l'Orchestre Symphonique de Montréal, 6ème symphonie de Beethoven</a> </a></p>

## Navigation en salle immersive

<iframe src="https://player.vimeo.com/video/522509890?color=ffffff&title=0&byline=0&portrait=0" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/522509890">Prototypage d'une expérience immersive</a></a></p>

## https://www.osm.ca/fr/osm-polyphonique/

![osmpoly  -image-no-border](img/OSM_Polyphonique.png){width=80%}

## Microphones mono et ambio

<iframe src="https://player.vimeo.com/video/680243754?h=6863b354ec" width="640" height="357" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/680243754">Hindelmith quintette à vent</a></p>

## Liens

* Logiciel SATIE (navigation sonore dans l'OSM)
 <p><a href="https://sat-metalab.gitlab.io/satie/">https://sat-metalab.gitlab.io/satie/</a></p>
* Jeu de données 6-ddl pour la recherche
 <p><a href="https://archive.org/details/savr_audio_dataset/">https://archive.org/details/savr_audio_dataset/</a></p>

nbouillot@sat.qc.ca

## captation acoustique pour rendus en 6-ddl

![](./img/savr_Maison_Symphonique_Acoustique_2021_02_02_20.jpg){width=40%}

Capture de réponses impulsionnelles à la Maison Symphonique 

## Simulation acoustique 6-ddl en direct

<iframe src="https://player.vimeo.com/video/484122810?color=ffffff&title=0&byline=0&portrait=0" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/484122810">Simulation acoustique en direct avec vaRays</a></a></p>

## Navigation acoustique 

<iframe src="https://player.vimeo.com/video/507255065?color=ffffff&title=0&byline=0&portrait=0" width="720" height="480" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
<p><a href="https://vimeo.com/507255065">Expérimentation technique avec vaRays et le projet Bretez</a></a></p>

## Hybride et en direct (2022-2026)

* Streaming multicanal
* Scénographie
* Espace immersif virtuel en ligne (le hub Satellite de la [SAT])
<p><a href="https://sat.qc.ca/fr/satellite#section">https://sat.qc.ca/fr/satellite</a></p>

